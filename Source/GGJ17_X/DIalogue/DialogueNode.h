// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "DialogueNode.generated.h"

class UDialogueNode;

USTRUCT(BlueprintType)
struct GGJ17_X_API FDialogueNodeOption
{
    GENERATED_BODY()

public:
    FDialogueNodeOption() : Text(FText::FromString("Empty")), AfterAnswerDelay(15.0f) {}

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FText Text;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float AfterAnswerDelay;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TAssetPtr<UDialogueNode> LinkToNode;
};

/**
 * 
 */
UCLASS(BlueprintType)
class GGJ17_X_API UDialogueNode : public UDataAsset
{
    GENERATED_BODY()

public:
    UDialogueNode();

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FText Text;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float OptionDuration;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FDialogueNodeOption> Options;

    //The option selected when none is picked
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FDialogueNodeOption StandartOption;

};
